/*
 *  parameter handling for cpbuilder.
 *  Copyright (C) 2007-2009 Junichi Uekawa
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 */

#ifndef __PARAMETER_H__
#define __PARAMETER_H__
#include "log.h"

#define MAX_CUSTOM_FILES 32

enum pbuilder_operation {
	pbuilder_do_nothing = 0,
	pbuilder_help,
	pbuilder_build,
	pbuilder_create,
	pbuilder_update,
	pbuilder_execute,
	pbuilder_login,
	pbuilder_dumpconfig
};

typedef struct pbuilderconfig {
	/* if you edit here, please add to parameter.c: dumpconfig */
	log_level log_level;
	int use_colors;
	int mountproc;
	int mountdev;
	int mountdevpts;
	int save_after_login;
	int debug;
	char *buildplace;  /* /var/cache/pbuilder/build/XXX.$$ */
	char *buildresult; /* /var/cache/pbuilder/result/ */
	char *basepath;	/* /var/cache/pbuilder/cow */
	char *mirror;
	char *distribution;
	char *components;
	char *extrapackages;
	char *othermirror;
	char *hookdir;
	char *debbuildopts;
	int binary_arch;
	int binary_indep;
	char *http_proxy;
	int allow_untrusted;

	/* files to be copied into the chroot,
	 * and copied out of the chroot */
	char *inputfile[MAX_CUSTOM_FILES + 1];
	char *outputfile[MAX_CUSTOM_FILES + 1];

	int buildresultuid;
	int buildresultgid;

	/* cow-specific options */
	int no_cowdancer_update;	/* --no-cowdancer-update */
	int debian_etch_workaround; /* --debian-etch-workaround */

	/* more qemu-isque options */
	char *kernel_image;
	char *initrd;
	char *smp;
	int memory_megs; /* megabytes of memory */
	char *arch;
	char *arch_diskdevice;

	enum pbuilder_operation operation;
} pbuilderconfig;

int load_config_file(const char *config, pbuilderconfig *pc);
int size_of_ntarray(char **buf);
int forkexeclp(const char *path, const char *arg0, ...);
int forkexecvp(char *const argv[]);
int parse_parameter(int ac, char **av, const char *keyword);
int cpbuilder_check_config(const struct pbuilderconfig *pc);
int cpbuilder_build(const struct pbuilderconfig *pc, const char *dscfile);
int cpbuilder_login(const struct pbuilderconfig *pc);
int cpbuilder_execute(const struct pbuilderconfig *pc, char **av);
int cpbuilder_update(const struct pbuilderconfig *pc);
int cpbuilder_help(void);
int cpbuilder_create(const struct pbuilderconfig *pc);

/*

The pbuilder command-line to pass

0: pbuilder
1: build/create/login etc.
offset: the next command

The last-command will be
PBUILDER_ADD_PARAM(NULL);

 */
#define MAXPBUILDERCOMMANDLINE 256
#define PBUILDER_ADD_PARAM(a) \
	if (offset < (MAXPBUILDERCOMMANDLINE - 1)) { \
		pbuildercommandline[offset++] = a; \
	} else { \
		pbuildercommandline[offset] = NULL; \
		log_printf(log_error, \
				   "pbuilder-command-line: Max command-line exceeded\n");      \
	}
extern char *pbuildercommandline[MAXPBUILDERCOMMANDLINE];
extern int offset;

/*

The debootstrap command-line to pass
Only used by qemubuilder

0: debootstrap
1: --arch
2: <architecture>
3: --foreign
debootstrap_param_offset: the next command

The last-command will be
DEBOOTSTRAP_ADD_PARAM(NULL);

 */
#define MAX_DEBOOTSTRAP_COMMAND_LINE 256
#define DEBOOTSTRAP_ADD_PARAM(a) \
	do { \
		if (debootstrap_param_offset < MAX_DEBOOTSTRAP_COMMAND_LINE - 1)       \
			debootstrap_command_line[debootstrap_param_offset++] = a;          \
		else { \
			debootstrap_command_line[debootstrap_param_offset] = NULL;         \
			log_printf( \
				log_error, \
				"debootstrap_command_line: Max command-line length exceeded"); \
		} \
	} while (0)

extern char *debootstrap_command_line[MAX_DEBOOTSTRAP_COMMAND_LINE];
extern int debootstrap_param_offset;

#endif

/*
 *  logging for cowdancer.
 *  Copyright (C) 2016 Jessica Clarke
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 *
 */

#ifndef __LOG_H__
#define __LOG_H__

#include <stdarg.h>
#include <stdio.h>

typedef enum log_level {
	log_debug = 0,
	log_info = 1,
	log_warn = 2,
	log_error = 3,
	/* Do not use on its own - OR it in */
	log_always_print = 0x100
} log_level;

#define LOG_LEVEL_MASK 0xff

typedef enum log_use_colors {
	log_use_colors_auto,
	log_use_colors_no,
	log_use_colors_yes
} log_use_colors;

log_level log_get_filter_level(void);

void log_set_filter_level(log_level filter_level_new);

log_use_colors log_get_use_colors(void);

log_use_colors log_get_use_colors_unresolved(void);

void log_set_use_colors(log_use_colors use_colors_new);

void log_perror(const char *s);

__attribute__((format(printf, 2, 3))) void
log_printf(log_level level, const char *format, ...);

void log_begin(log_level level);

__attribute__((format(printf, 2, 3))) void
log_middle(log_level level, const char *format, ...);

__attribute__((format(printf, 2, 0))) void
log_vmiddle(log_level level, const char *format, va_list args);

void log_end(log_level level);

#endif /* !__LOG_H__ */
